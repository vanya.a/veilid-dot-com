# Contributing to Veilid

Before you get started, please review our [Code of Conduct](./code_of_conduct.md).
We're here to make things better, and we cannot do that without treating each other with respect.

This document covers:
- [Code Contributions][#code-contributions]
- [Contributions Without Writing Code][#contributions-without-writing-code]
- [Bug Reports][#bug-reports]

## Code Contributions

### Technical Overview

Content is stored in markdown files, and rendered to a static site using
[cecil.app](https://cecil.app/). Templating is done via [Twig](https://twig.symfony.com/).

Hosting is provided by [Netlify](https://www.netlify.com/).

Styling uses [Bootstrap](https://getbootstrap.com/) and [Sass](https://sass-lang.com/).

[JQuery](https://jquery.com/) is available (but not yet used),
as we may need more UI functionality in the future.

### Quickstart

1. Install [PHP 7.4 or higher](https://www.php.net/manual/en/install.php)
   as a dependency for Cecil.
	- On Arch, you will also need php-gd
		- `sudo pacman -S php php-gd`
		- Enable php-gd by uncommenting `extension=gd` in `/etc/php/php.ini` near line 925
	- On [Debian-based systems](https://www.php.net/manual/en/install.unix.debian.php)
	- You will probably also need php-gd - we haven't tested this yet
2. [Download and install Cecil](https://cecil.app/download/)
3. Fork and/or clone the repository
	- Via SSH: `git@gitlab.com:veilid/veilid-dot-com.git`
	- Via HTTPS: `https://gitlab.com/veilid/veilid-dot-com.git`
4. In the project directory, run `cecil serve` to run the site locally on port 8000
5. Open `http://localhost:8000/` in your browser to view it 

As you make changes to the code, Cecil will reload the website for you so you can see your changes
as you work. 

### Directory Structure

```
- pages/           - Site content, in markdown files
- static/          - Static assets, served from the site root
- themes/velid/    - The site theme
    - assets/scss/ - SCSS source files
    - layout/      - Twig templates. Not all are in use
    - static/      - Static assets, also served from the site root
```

### Style Guide

This section needs expansion.

In general, commit early and often, and mirror existing styles. Just like in UI design, a great
developer experience is derived from *consistency* and the *prinicple of least surprise*.

### Code Contribution Do's & Don'ts:

Keeping the following in mind gives your contribution the best chance of landing.

When creating a *Merge Request*:

- **Do** create a [feature branch] to work on instead of working directly on `main`. This helps to:
	- Protect the process.
	- Ensures users are aware of commits on the branch being considered for merge.
	- Allows for a location for more commits to be offered without mingling with other contributor changes.
	- Allows contributors to make progress while a MR is still being reviewed.
- **Do** follow the [50/72 rule] for Git commit messages.
- **Do** target your merge request to the **main branch**.
- **Do** specify a descriptive title to make searching for your merge request easier.
- **Do** list [verification steps] so your code is testable.
- **Do** reference associated issues in your merge request description.
- **Don't** leave your merge request description blank.
- **Don't** abandon your merge request. Being responsive helps us land your code faster.
- **Don't** submit unfinished code.

When submitting a *Bug Fix*:

- **Do** include reproduction steps in the form of [verification steps].
- **Do** link to any corresponding issues in your commit description.

## Contributions Without Writing Code

There are numerous ways you can contribute to the growth and success of the Veilid project without writing code:

- Submit bugs as well as feature/enhancement requests. Letting us know you found a bug, have an idea for a new feature, or see a way we can enhance existing features is just as important and useful as writing the code related to those things. Send us detailed information about your issue or idea:
	- Features/Enhancements: Describe your idea. If you're able to, sketch out a diagram or mock-up.
	- Bugs: Please be sure to include the expected behavior, the observed behavior, and steps to reproduce the problem. Please be descriptive about the environment you've installed your node or application into. 
- [Help other users with open issues]. Sometimes all an issue needs is a little conversation to clear up a process or misunderstanding. Please keep the [Code of Conduct](./code_of_conduct.md) in mind.
- Help other contributors test recently submitted merge requests. By pulling down a merge request and testing it, you can help validate new code contributions for stability and quality.
- Report a security or privacy vulnerability. Please let us know if you find ways in which Veilid could handle security and/or privacy in a different or better way. Surely let us know if you find broken or otherwise flawed security and/or privacy functions. You can report these directly to security@veilid.org.
- Add or edit documentation. Documentation is a living and evolving library of knowledge. As such, care, feeding, and even pruning is needed from time to time. If you're a non-native english speaker, you can help by replacing any ambiguous idioms, metaphors, or unclear language that might make our documentation hard to understand.

When submitting *Bug Reports* or *Issues*:

- **Do** write a detailed description of your bug and use a descriptive title.
- **Do** include reproduction steps, stack traces, and anything that might help us fix your bug.
- **Don't** file duplicate reports. Search open issues for similar bugs before filing a new report.
- **Don't** attempt to report issues on a closed PR. New issues should be openned against the `main` branch.

Please report vulnerabilities in Veilid directly to security@veilid.org.

## Thank You!

If you're looking for more guidance, please join us on the [Veilid Discord].

Together, we will build something truly remarkable.

This contributor guide is inspired by the contribution guidelines of the [Metasploit Framework](https://github.com/rapid7/metasploit-framework/blob/master/CONTRIBUTING.md) project found on GitHub.

[Help other users with open issues]:https://gitlab.com/veilid/veilid-dot-com/-/issues
[50/72 rule]:http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
[feature branch]:https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html
[verification steps]:https://docs.gitlab.com/ee/user/markdown.html#task-lists
[Veilid Discord]:https://discord.gg/DmEGxyA87N
