---
title: Demo Lab
description: TC and Veggie demonstrated how to communicate with Veilid
weight: 3
layout: subpage
---

### Veilid

#### A Demo Lab

##### Who

TC Johnson &amp; Deth Veggie

##### Where &amp; When

Saturday August 12, 10:00 – 11:55, Committee Boardroom, Forum

##### What

Veilid is a new, distributed communication protocol developed by Cult of the Dead Cow's Dildog (of BO2K fame). This p2p, E2EE, distributed protocol is being released at Defcon 31, fully open source, and with an example app called Veilid Chat. These demos will cover setting up an identity, connecting to others, deploying heavy nodes to support the network, and contributing to the project.

[Official Forum Page](https://forum.defcon.org/node/246329)