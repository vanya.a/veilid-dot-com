---
title: DefCon
description: Veilid is launching at DefCon 31 in Las Vegas
layout: subpage
menu:
  main:
    weight: 2
weight: 1
---

<div class="row g-5">
  <div class="col-12 col-md-7">
    <p>We're premiering Veilid at Def Con 31 in Las Vegas!</p>
    <p>
      The <a href="/defcon/talk/">launch talk</a> and 
      <a href="/defcon/lab/">demo lab</a> targeted more at developers and other technical folks.</p>
    <p><a href="/defcon/party/">The party</a> is for anyone attending the conference.</p>
  </div>
  <div class="col-12 col-md-5">
    <div class="defcon">
      <a href="https://defcon.org/html/defcon-31/dc-31-index.html"><img src="/img/defcon31-logo-gradient.png" alt="DefCon 31 Logo" class="img-fluid" /></a>
    </div>
  </div>
</div>



