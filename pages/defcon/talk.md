---
title: Launch Talk
description: Dildog and Medus4 go over the how's and why's of Veilid in "The Internals of Veilid"
weight: 2
layout: subpage
---

### The Internals of Veilid

#### A New Decentralized Application Framework

##### Who
Christien 'DilDog' Rioux and Katelyn 'Medus4' Bowden at DefCon 31

[Launch-Slides-Veilid.pdf](/Launch-Slides-Veilid.pdf)

##### Where &amp; When
Friday, August 11 at 09:00; Track 1

##### What

Attend our presentation of Veilid: an open-source, peer-to-peer, mobile-first networked application framework. Talk covers how it works as a protocol, structures, cryptography, and how to write applications.

[Official Forum Page](https://forum.defcon.org/node/246124)