---
title: Privacy Policy
description: The privacy policy for VeilidChat
weight: 2
layout: subpage
---

### VeilidChat Privacy Policy

Last Revised 2023 Aug 3

This privacy notice for the Veilid Foundation ("Veilid," "we," "us," or "our"), describes how and why we might collect, store, use, and/or share ("process") your information when you use our services ("Services"), such as when you:

- Download and use our mobile application (VeilidChat), or any other application of ours that links to this privacy notice in the future.

Reading this privacy notice will help you understand your privacy rights and choices. If you do not agree with our policies and practices, please do not use VeilidChat. If you still have any questions or concerns, please contact us at [support@veilid.com](mailto:support@veilid.com).

<h4 id="summary">Summary of Key Points</h4>

- VeilidChat is a messaging application built on a decentralized, distributed, and end-to-end fully encrypted framework.
- VeilidChat is a proof-of-concept application in beta-testing and you must receive an invitation to activate the application.
- VeilidChat will never ask you for contact information, personal information, or financial information.
- When you register and use VeilidChat, any information entered into the app is either stored locally on your device, or encrypted and distributed in a method that is inaccessible and irretrievable by the Veilid Foundation (the creators of the application).
- We do not collect or process any personal information, nor do we have the capability to collect or process any personal information from VeilidChat.
- We do not collect or process sensitive personal information, nor do we have the capability to collect or process any sensitive information from VeilidChat.
- We do not receive or transmit any information to or from third parties.
- The Veilid Framework processes encrypted and distributed information to provide and administer services for VeilidChat.  
- We do not share personal information. Because of the encrypted and distributed nature of information entered into the VeilidChat app, we are unable to access your personal information.
- How do we keep your information safe? We have organizational and technical processes and procedures in place to protect your personal information. However, no electronic transmission over the internet or information storage technology can be guaranteed to be 100% secure, so we cannot promise or guarantee that unauthorized third parties will not be able to defeat our security and improperly collect, access, steal, or modify your information.
- VeilidChat will provide you with a recovery pass-phrase to use in the event that your app is deleted or your device is no longer available/functional for you. If you cannot access this pass-phrase, you will have to create a new account. The Veilid Foundation is unable to restore your account through any means.
- How do you exercise your rights? The easiest way to exercise your rights is to delete VeilidChat. The Veilid Foundation is a non-profit organization that does not collect, process, or use any of your information. You may contact us by visiting https://veilid.com/contact, or by emailing [support@veilid.com](mailto:support@veilid.com). We will consider and act upon any request in accordance with applicable data protection laws.

<h4 id="table-of-contents">Table of Contents</h4>

1. [What Information Do We Collect?](#section-1)
1. [How Do We Process Your Information?](#section-2)
1. [What Legal Bases Do We Rely On To Process Your Personal Information?](#section-3)
1. [When And With Whom Do We Share Your Personal Information?](#section-4)
1. [How Long Do We Keep Your Information?](#section-5)
1. [How Do We Keep Your Information Safe?](#section-6)
1. [Do We Collect Information From Minors?](#section-7)
1. [What Are Your Privacy Rights?](#section-8)
1. [Controls For Do-not-track Features](#section-9)
1. [US State-specific Privacy Rights](#section-10)
1. [Do We Make Updates To This Notice?](#section-11)
1. [How Can You Contact Us About This Notice?](#section-12)
1. [How Can You Review, Update, Or Delete The Data We Collect From You?](#section-13)
1. [Cookie And Data Analytics Disclosures](#section-14)

<h4 id="section-1">1. What information do we collect?</h4>

VeilidChat collects no information from you. VeilidChat is designed to not collect your information and to operate in an encrypted, distributed manner, which means we cannot access your information even if we wanted to (which we don’t). Any information you enter into VeilidChat on your device is inaccessible to us.

The Veilid Foundation only collects personal information that you voluntarily provide to us when you request an invitation to the beta version of the application. This will only be the contact information necessary for us to send you an invitation. This process is managed by the Apple App Store and the Veilid Foundation only retains this information long enough to provide it to the Apple App Store to generate an invitation.

<h4 id="section-2">2. How do we process your information?</h4>

We don’t. VeilidChat does not collect your information, nor are we able to collect it. Administration of, improvements to, and changes to the VeilidChat application will be made without any processing of personal information. The Veilid Foundation will never ask you for personal, sensitive, or financial information.

If you provide any information to us at [support@veilid.com](mailto:support@veilid.com), though our Discord server, through our website at https://veilid.com/contact, or directly via postal mail to our foundation, we will only process that information to answer your questions or attempt to support your issues, with two major exceptions:

- Legal Obligations. We may process information sent to us via email, our website, or via postal mail where we believe it is necessary for compliance with our legal obligations, such as to cooperate with a law enforcement body or regulatory agency, exercise or defend our legal rights, or disclose your information as evidence in litigation in which we are involved.
- Vital Interests. We may process information sent to us via email, our website, or via postal mail where we believe it is necessary to protect your vital interests or the vital interests of a third party, such as situations involving potential threats to the safety of any person.

<h4 id="section-3">3. What legal bases do we rely on to process uour information?</h4>

VeilidChat does not process any personal information. Because the Veilid Foundation cannot access information entered into VeilidChat in any way, shape, or form, the Veilid Foundation does not process your information.

As for information sent to the Veilid Foundation via email, Discord server, postal mail, or through our website, we only process your personal information when we believe it is necessary and we have a valid legal reason (i.e.,legal obligations) to do so under applicable law, to comply with laws, to protect your rights, or to fulfill our legitimate business interests.

<strong>If you are located in the EU or UK, this section applies to you.</strong>

The General Data Protection Regulation (GDPR) and UK GDPR require us to explain the valid legal bases we rely on in order to process your personal information. VeilidChat does not collect or process personal information. For any information that is sent directly to the Veilid Foundation, we may rely on the following legal bases to process your personal information:

- Consent. The Veilid Foundation may process your information if you have given us specific permission (i.e. consent) to use your personal information for a specific purpose. This is generally limited to contacting us and requesting an invite code to the beta version of the application, however, the Veilid Foundation does not store this information. Apple, through the App Store, will manage the invitation process. Your contact information cannot be linked to your VeilidChat account by the Veilid Foundation. You can withdraw your consent to Apple at any time.
- Legal Obligations. We may process your information where we believe it is necessary for compliance with our legal obligations, such as to cooperate with a law enforcement body or regulatory agency, exercise or defend our legal rights, or disclose your information as evidence in litigation in which we are involved.
- Vital Interests. We may process your information where we believe it is necessary to protect your vital interests or the vital interests of a third party, such as situations involving potential threats to the safety of any person.

<strong>If you are located in Canada, this section applies to you.</strong>

VeilidChat does not collect or process personal information. For any information that is sent directly to the Veilid Foundation, we may rely on the following legal bases to process your personal information:

We may process your information if you have given us specific permission (i.e., express consent) to use your personal information for a specific purpose, or in situations where your permission can be inferred (i.e., implied consent). This is generally limited to contacting us and requesting an invite code to the beta version of the application. Apple, through the App Store, will manage the invitation process. Your contact information cannot be linked to your VeilidChat account by the Veilid Foundation. You can withdraw your consent to Apple at any time.

- In some exceptional cases, we may be legally permitted under applicable law to process your information without your consent, including, for example:
- If collection is clearly in the interests of an individual and consent cannot be obtained in a timely way
- For investigations and fraud detection and prevention
- If it is contained in a witness statement and the collection is necessary to assess, process, or settle an insurance claim
- For identifying injured, ill, or deceased persons and communicating with next of kin
- If we have reasonable grounds to believe an individual has been, is, or may be victim of financial abuse
- If it is reasonable to expect collection and use with consent would compromise the availability or the accuracy of the information and the collection is reasonable for purposes related to investigating a breach of an agreement or a contravention of the laws of Canada or a province
- If disclosure is required to comply with a subpoena, warrant, court order, or rules of the court relating to the production of records
- If it was produced by an individual in the course of their employment, business, or profession and the collection is consistent with the purposes for which the information was produced
- If the collection is solely for journalistic, artistic, or literary purposes
- If the information is publicly available and is specified by the regulations


<h4 id="section-4">4. When and with whom do we share your personal information?</h4>

In no way, shape, or form do we ever share your personal information. We cannot share what we do not retain, or what we cannot access.

- Business Transfers. In the event of any merger, sale, or acquisition of any portion of the Veilid Foundation, the Veilid Foundation will provide updates on our website or through application updates for the VeilidChat application through the App Store. Because the Velid Foundation does not collect any personal information, we have no other methods to provide this information to you.

<h4 id="section-5">5. How long do we keep your information?</h4>

We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice unless otherwise required by law. However, in most cases, we are incapable of retaining information because we do not collect it.

The Veilid Foundation will only keep your personal information for as long as it is necessary for the purposes set out in this privacy notice, unless a longer retention period is required by law. VeilidChat will retain a local copy of your information (accessible only to your device and the people you directly share information with) for as long as you utilize the application. In future iterations of the application, there will be a “delete messages after a certain period of time” functionality, but for the beta testing proof-of-concept, this functionality has not been introduced.

<h4 id="section-6">6. How do we keep your information safe?</h4>

We have implemented appropriate and reasonable technical and organizational security measures designed to protect the security of any personal information we process. However, despite our safeguards and efforts to secure your information, no electronic transmission over the Internet or information storage technology can be guaranteed to be 100% secure, so we cannot promise or guarantee that any unauthorized third parties will not be able to defeat our security and improperly collect, access, steal, or modify your information.

Although we will do our best to protect your personal information, transmission of personal information to and from Velid Chat is at your own risk. You should only access VeilidChat within a secure environment. If you lose access to your device, the Velid Foundation cannot access or delete information stored there.


<h4 id="section-7">7. Do we collect information from minors?</h4>

We do not collect information from or market to anyone. If you suspect that someone underage is violating the Apple App Store terms of service by downloading VeilidChat, please contact Apple at Support.Apple.com.

<h4 id="section-8">8. What are your privacy rights?</h4>

In some regions (like the EEA, UK, and Canada), you have certain rights under applicable data protection laws. These may include the right (i) to request access and obtain a copy of your personal information, (ii) to request rectification or erasure; (iii) to restrict the processing of your personal information; and (iv) if applicable, to data portability. In certain circumstances, you may also have the right to object to the processing of your personal information.

However, due to the unique nature of VeilidChat, the only information available is stored locally on the user’s device and is inaccessible by the Veilid Foundation. VeilidChat does not collect or process any personal information. If a user wishes to exercise their data rights, they may copy any data within the VeilidChat application and export it themselves, or delete the application from their device. The Veilid Foundation has no access to any user’s data.

We will consider and act upon any request in accordance with applicable data protection laws.

If you are located in the EEA or UK and you believe we are unlawfully processing your personal information, you also have the right to complain to your Member State data protection authority or UK data protection authority.

If you are located in Switzerland, you may contact the Federal Data Protection and Information Commissioner.

##### Account Information

If you would at any time like to review or change the information in your account or terminate your account, you can:

- Log in to your account settings and update your user account.
- Delete the application from your device.

Upon your decision to delete the application from your device, it will no longer be able to be accessed by anyone, including the creators of the application. Any data still in existence will be encrypted, distributed, and inaccessible by any party.

However, we may retain some information that is sent directly to the Veilid Foundation via email, our website, our Discord server, or via post mail in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our legal terms and/or comply with applicable legal requirements.

If you have questions or comments about your privacy rights, you may email us at [support@veilid.com](mailto:support@veilid.com).


<h4 id="section-9">9. Controls for Do-Not-track Features</h4>

Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track ("DNT") feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. VeilidChat is compliant with this technology because we do not monitor, track, collect, or process any information.


<h4 id="section-10">10. US State-Specific Privacy Rights</h4>

- California (knows how to party):
  - We do not sell, rent, or share information with anyone, including third parties who engage in direct marketing activities. We do not collect or share your information with anyone, for any reason. No marketing, no third party sharing, nothing.
  - VeilidChat has no  ability to post public information. If you are under 18 years of age, reside in California, and have a registered account with VeilidChat, you can delete the application on your device and that will serve to exercise your rights to deletion.
  - Please note that the Veilid Foundation and the VeilidChat application HAVE NOT collected, rented, shared, or sold any personal information from any individual, and especially not within the past 12 months.
- De-identified Data Disclosure (CA, CO, CT, UT, VA and soon to be more)
  - We may use de-identified data in certain circumstances. In those circumstances, not only do we never attempt to re-identify that data, but we are completely unable to re-identify any data because of our encryption and distributed data frameworks.
- Profiling Disclosure (Hi Colorado!)
  - We do not engage in profiling of consumers in any way, shape, or form. We especially don’t do it in a way that supports automated decisions that might have a legal or significant effect on anyone.


<h4 id="section-11">11. Do we make updates to this notice?</h4>

We will update this notice as necessary to stay compliant with relevant laws and as it relates to new features or products available.

The updated version will be indicated by an updated "Revised" date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes on the Veilid Foundation’s website, or by providing an updated policy through the Apple App Store as part of any version updates. Because we do not collect your information, we are unable to directly contact you. We encourage you to review this privacy notice frequently to be informed of how we are protecting your information.

<h4 id="section-12">12. How can you contact us about this notice?</h4>

If you have questions or comments about this notice, you may email us at [support@veilid.com](mailto:support@veilid.com) or contact us by post at:

<pre>
Veilid Foundation
PO BOX 1593
Madison AL
35758
</pre>


<h4 id="section-13">13. How can you review, update, or delete the data we collect from you?</h4>

The only effective method to review, update, or delete any of your data is locally on the VeilidChat application on your device. The Veilid Foundation is unable to collect or access your information through the VeilidChat application and thus does not have the ability to review, update, or delete information that we do not possess.


<h4 id="section-14">14. Cookie and Data Analytics Disclosure</h4>

We don’t use cookies. We don’t use data analytics. You can check on our website. 
