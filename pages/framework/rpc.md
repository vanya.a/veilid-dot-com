---
title: RPC Protocol
description: An overview of the RPC protocol used in Veilid
weight: 4
layout: subpage
---

Strong, appropriate, cryptography choices are essential to the functioning of Veilid.

Veilid provides applications guarantees about how data is handled on the wire and at rest.

Cryptosystems were chosen that work well together and provide a balance of speed and cryptographic hardness.

### RPC Summary

<ul>
  <li>
    <h4>Schema Language is Cap'n Proto</h4>
    <a href="https://capnproto.org/">Cap’n Proto</a> is designed for deserialization speed and schema evolution. Flexible and well supported in Rust.
  </li>
  <li>
    <h4>RPC is fully in-schema and documented</h4>
    Both ‘Question/Answer’ and ‘Statement’ RPC modes are supported. All schema ﬁelds are documented.
  </li>
  <li>
    <h4>RPC fully supports Private Routing</h4>
    All private routing structures are expressed in the RPC schema itself, no magic encrypted blobs.
  </li>
  <li>
    <h4>Schema Evolution is built-in</h4>
    Fields can be added and removed with full backward and forward compatibility. New features won’t break older Veilid nodes.
  </li>
  <li>
    <h4>RPC Schema is cryptography-independent</h4>
    As cryptosystems change, the language spoken by Veilid nodes remains the same.
  </li>
</ul>

### Distributed Hash Table

Distributed Hash Tables are a way of storing data in records that have keys that are close to nodes in the network.

#### DHT Is Just ‘Search’
It may look complicated, but all the DHT algorithms out there are just ‘search’ algorithms. Finding data that is stored on some node somewhere out there.

#### Improving Search
We built a better DHT by making both search and data locality more relevant. Veilid synchronizes popular data when nodes come and go from the network.

<img src="/img/dht-diagram.png" alt="a tree diagram for the search ability" class="img-fluid lightbox">

Locating a node by its ID. Here the node with the prefix 0011 finds the node with the prefix 1110 by
successively learning of and querying closer and closer nodes. The line segment on top represents the 
space of 160-bit IDs, and shows how the lookups coverge to the target node. Below we illustrate RPC messages
made by 1110. The first RPC is to node 101, already known to 1110. Subsequent RPCs are to nodes return by the
previous RPC. 

#### DHT Schema
Veilid DHT is built using GetValue and SetValue RPC operations. Nodes can opt out of DHT storage if they do not want to participate.

Veilid DHT records have schemas that deﬁne subkeys that are individually addressable and can have multiple writers.

DHT record subkeys have sequence numbers and are eventually consistent across multiple writes and background synchronizations.

<div class="row gx-5 gy-3 mb-3">
  <div class="col-12 col-lg-6">
    <figure class="h-100">
        <img src="/img/dht-dflt-framed.png" alt="a diagram showing key-value pairs" class="img-fluid lightbox">
        <figcaption>
          <p>Veilid Default DHT Schema - DFLT</p>
          <p>To zoom in on the details, <a href="/img/dht-dflt-framed.png">view the image directly</a>.</p>
        </figcaption>
    </figure>
  </div>
  <div class="col-12 col-lg-6">
    <figure class="h-100">
        <img src="/img/dht-smpl.png" alt="a diagram showing key-value pairs, but with more fields" class="img-fluid lightbox">
        <figcaption>
          <p>Veild Simple DHT Schema - SMPL</p>
          <p>To zoom in on the details, <a href="/img/dht-smpl.png">view the image directly</a>.</p>
        </figcaption>
    </figure>
  </div>
</div>



<div class="focus-text">
  <p>The DHT gives you full control over your data</p>
  <p>Our DHT is not based on a blockchain or a coin </p>
  <p>Popular data becomes more available automatically </p>
</div>
