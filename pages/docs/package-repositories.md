---
title: Package Repositories
description: Veilid has Rust, Python, Debian, and Fedora packages available.
weight: 4
layout: subpage
---

To run a "headless" Veilid node, you can install `veilid-server` and `veilid-cli` from repositories.

<h3>Debian Based Systems</h3>

<div class="code-snippet">
  <input aria-label="Get Keys" type="text" readonly value="wget -O- https://packages.veilid.net/gpg/veilid-packages-key.public | sudo gpg --dearmor -o /usr/share/keyrings/veilid-packages-keyring.gpg">
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>

<p>Note: The key's fingerprint is <code>516C76D1E372C5C96EE54E22AE0E059BC64CD052</code></p>

<div class="code-snippet">
  <input aria-label="Add Keys" type="text" readonly value='echo "deb [arch=amd64 signed-by=/usr/share/keyrings/veilid-packages-keyring.gpg] https://packages.veilid.net/apt stable main" > /etc/apt/sources.list.d/veilid.list'>
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>

<div class="code-snippet">
  <input aria-label="Run Updates" type="text" readonly value="apt update">
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>

<div class="code-snippet">
  <input aria-label="Install the headless and command line utilities" type="text" readonly value="apt install veilid-server veilid-cli">
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>


<h3>Fedora Based Systems</h3>

<div class="code-snippet">
  <input aria-label="Add repo" type="text" readonly value="yum-config-manager --add-repo https://packages.veilid.net/rpm/veilid-rpm-repo.repo">
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>
<div class="code-snippet">
  <input aria-label="Install the headless and command line utilities" type="text" readonly value="dnf install veilid-server veilid-cli">
  <button class="btn btn-secondary"><span>Copy</span></button>
</div>

<h3>PyPi</h3>
<p><a href="https://pypi.org/project/veilid/">https://pypi.org/project/veilid/</a></p>
