---
title: Press
description: Information for the press and media about Veilid
layout: subpage
weight: 4
---

### Press Kit

PDF press summary: <a href="/Veilid-Framework-Press-Kit.pdf">Veilid Framework Press Kit</a>, 20.6 kB

### Press Contact
Press and media inquiries should be sent to <a href="mailto:press@veilid.org">press@veilid.org</a>

### In The News

<ul>
  <li>“America’s Original Hacking Supergroup Creates a Free Framework to Improve App Security.” <i>Engadget</i>, <a href="https://www.engadget.com/americas-original-hacking-supergroup-creates-a-free-framework-to-improve-app-security-190043865.html">https://www.engadget.com/americas-original-hacking-supergroup-creates-a-free-framework-to-improve-app-security-190043865.html</a>. Accessed 11 Aug. 2023.</li>
  <li><i>Cult of Dead Cow Hacktivists Design Encryption System for Mobile Apps - Play Crazy Game</i>. 2 Aug. 2023, <a href="https://playcrazygame.com/blog/2023/08/02/cult-of-dead-cow-hacktivists-design-encryption-system-for-mobile-apps/">https://playcrazygame.com/blog/2023/08/02/cult-of-dead-cow-hacktivists-design-encryption-system-for-mobile-apps/</a>.</li>
  <li>“Cult of the Dead Cow Launches Encryption Protocol to Save Your Privacy.” <i>Gizmodo</i>, 2 Aug. 2023, <a href="https://gizmodo.com/cult-of-the-dead-cow-launches-veilid-encryption-project-1850699803">https://gizmodo.com/cult-of-the-dead-cow-launches-veilid-encryption-project-1850699803</a>.</li>
  <li>“Cult of the Dead Cow Wants to Save Internet Privacy with a New Encryption Protocol.” <i>Yahoo News</i>, 2 Aug. 2023, <a href="https://news.yahoo.com/cult-dead-cow-wants-save-203200064.html">https://news.yahoo.com/cult-dead-cow-wants-save-203200064.html</a>.</li>
  <li>Hemant, Kumar. “Hacktivist Group CDc to Unveil Veilid Encryption for Privacy-First Apps.” <i>Candid.Technology</i>, 2 Aug. 2023, <a href="https://candid.technology/veilid-encryption-cdc-cult-of-dead-cow/">https://candid.technology/veilid-encryption-cdc-cult-of-dead-cow/</a>.</li>
  <li>Johnson, Donovan. “Cult of the Dead Cow Develops Coding Framework for Privacy-Focused Apps.” <i>Fagen Wasanni Technologies</i>, 2 Aug. 2023, <a href="https://fagenwasanni.com/ai/cult-of-the-dead-cow-develops-coding-framework-for-privacy-focused-apps/106537/">https://fagenwasanni.com/ai/cult-of-the-dead-cow-develops-coding-framework-for-privacy-focused-apps/106537/</a>.</li>
  <li>Long, Heinrich. “Prominent Hacktivists to Launch Secure Messaging Framework Veilid.” <i>RestorePrivacy</i>, 5 Aug. 2023, <a href="https://restoreprivacy.com/prominent-hacktivists-to-launch-secure-messaging-framework-veilid/">https://restoreprivacy.com/prominent-hacktivists-to-launch-secure-messaging-framework-veilid/</a>.</li>
  <li>Menn, Joseph. “Hacking Group Plans System to Encrypt Social Media and Other Apps.” <i>Washington Post</i>, 2 Aug. 2023. <i>www.washingtonpost.com</i>, <a href="https://www.washingtonpost.com/technology/2023/08/02/encryption-dead-cow-cult-apps-def-con/">https://www.washingtonpost.com/technology/2023/08/02/encryption-dead-cow-cult-apps-def-con/</a>.</li>
  <li>RHC, Redazione. “Cult of the dead Cow presenta il protocollo Veilid. Una nuova alternativa al ‘capitalismo della sorveglianza.’” <i>Red Hot Cyber</i>, 3 Aug. 2023, <a href="https://www.redhotcyber.com/post/cult-of-the-dead-cow-presenta-il-protocollo-veilid-una-nuova-alternativa-al-capitalismo-della-sorveglianza/">https://www.redhotcyber.com/post/cult-of-the-dead-cow-presenta-il-protocollo-veilid-una-nuova-alternativa-al-capitalismo-della-sorveglianza/</a>.</li>
</ul>