---
title: Take Back Control
description: Veilid is an open-source, distributed application framework.
menu:
  main:
    weight: 1
layout: index
---


<p class="focus-text highlighted">
  Veilid is an 
    <span class="highlighter-1">open-source</span>, 
    <span class="highlighter-2">peer-to-peer</span>, 
    <span class="highlighter-3">mobile-ﬁrst</span>, 
    <span class="highlighter-4">networked</span> 
  application framework.
</p>

<p>Veilid (pronounced Vay-Lid, from 'Valid and Veiled Identification')</p>
<p>
  Veilid allows anyone to build a distributed, private app. Veilid gives users the privacy to opt out of 
  data collection and online tracking. Veilid is being built with user experience, privacy, and safety as our top priorities.
  It is open source and available to everyone to use and build upon.
</p>
<p>
  Veilid goes above and beyond existing privacy technologies and has the potential to completely change the way
  people use the Internet. Veilid has no profit motive, which puts us in a unique position to promote ideals
  without the compromise of capitalism.
</p>
<p class="focus-text">
  We built Veilid because when the Internet was young and new, we viewed it as an endless and open realm of possibility.
</p>
<p>
  Instead, the Internet we know now has been heavily commercialized, with users and their data being the most 
  sought-after commodity. The only ways to opt-out of becoming the product for billionaires to exploit are either 
  too technical for the average user, or to simply not go online.
</p>
<p>
  We don't believe that is fair; we still haven't given up our dream for the entire Internet to be free 
  and accessible without trading privacy to use it.
</p>
<p class="focus-text">
  We believe that everyone should be able to forge relationships, learn, create, and build online &mdash; without being monetized. 
</p>
<p>
  With Veilid, the user is in control, in a way that is approachable and friendly, regardless of technical 
  ability. We want to give the world the Internet we should have had all along.
</p>



<div class="row gx-5 gy-3 mt-5">
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-info btn-lg w-100" href="/framework">Technical Details</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn bgv-fuschia btn-lg w-100" href="/chat">VeilidChat</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-dark btn-lg w-100" href="/code">Code</a>
  </div>
  <div class="col-12 col-md-6 col-lg-3">
  <a class="btn btn-primary btn-lg w-100" href="/discord">Discord</a>
  </div>
</div>

<div class="row gx-5 gy-3 mt-3 justify-content-center">
  <div class="col-12 col-md-6">
  <a class="btn btn-success btn-lg w-100 text-white" href="/donate">Donate</a>
  </div>
</div>

