---
title: Web Text
description: How to customize your browser text appearance; based on the WAI customize design document.
layout: index
exclude: true
---

## Change Text Size with Zoom

<p>Most web browsers let you increase and decrease the size of text, images, and other web page content with "zoom" features. Some browsers let you choose to zoom only the text size.</p>
<p>To change the zoom in most browsers, press the following two keys at the same time:</p>
<ul>
  <li>In Windows, Linux, and Chrome OS:
    <ul>
      <li>To zoom bigger: "Ctrl" and "+" keys</li>
      <li>To zoom smaller: "Ctrl" and "-" keys</li>
    </ul>
  </li>
  <li>Mac OS:
    <ul>
      <li>To zoom bigger: "⌘" and "+" keys</li>
      <li>To zoom smaller: "⌘" and "-" keys</li>
    </ul>
  </li>
</ul>

Browsers provide specific guidance on different ways to change the page zoom or text-only zoom:

* [Google Chrome - Change text, image, and video sizes (zoom)](https://support.google.com/chrome/answer/96810)
* [Apple Safari - Zoom in on webpages](https://support.apple.com/guide/safari/zoom-in-on-webpages-ibrw1068/mac)
* [Mozilla Firefox - Font size and zoom](https://support.mozilla.org/en-US/kb/font-size-and-zoom-increase-size-of-web-pages)
* [Opera - Zoom](https://help.opera.com/en/latest/browser-window/#zoom)
* [Internet Explorer - Ease of access options](https://support.microsoft.com/en-us/help/17456/windows-internet-explorer-ease-of-access-options)
* [Microsoft Edge - Ease of Access in Microsoft Edge](https://support.microsoft.com/en-gb/help/4000734/windows-10-microsoft-edge-ease-of-access)
* [Vivaldi - Zooming options in Vivaldi](https://help.vivaldi.com/article/zooming-options-in-vivaldi/)

## Other Text and Color Changes

Some browsers provide functionality to set different aspects of font and color in the default view.

* [Mozilla Firefox - Change the fonts and colors websites use](https://support.mozilla.org/en-US/kb/change-fonts-and-colors-websites-use)
* [Opera - Look and feel > Fonts](https://help.opera.com/en/presto/look-and-feel/#fonts)
* [Microsoft Internet Explorer - Ease of Access Options](https://support.microsoft.com/en-us/help/17456/windows-internet-explorer-ease-of-access-options)

Other text and color settings are available in Reader View.

## Reader View

Most browsers offer a "Reader View" or "Reading View" that shows just the main content; it gets rid of navigation, ads, etc. Some browsers let you set the text font, text size, text color, background color, and line spacing in Reader View.

* [Apple Safari - Hide ads when reading articles](https://support.apple.com/en-ca/guide/safari/hide-ads-when-reading-articles-sfri32632/mac)
* [Mozilla Firefox - Reader View for clutter-free web pages](https://support.mozilla.org/en-US/kb/firefox-reader-view-clutter-free-web-pages)
* [Microsoft Edge - Change font style and size for Reading view in Microsoft Edge](https://support.microsoft.com/en-us/help/4028023/microsoft-edge-change-font-style-and-size-for-reading-view)
* [Vivaldi - Reader View](https://help.vivaldi.com/article/reader-view/)

## Advanced Options

There are many browser extensions and add-ons that provide additional control over how the browser displays text and other content.

While most browsers no long support user style sheets, extensions provide similar advanced user control. For example, the Stylus extension is available for several major browsers.

### Note: No Endorsement

We do not endorse specific web browsers or extensions and does not recommend one over another. 
While some common browsers are included in this page, mention of a specific browser does not imply 
endorsement or recommendation.


### About This Page

This page is based off the [WAI customize design document](https://github.com/w3c/wai-customize-design/blob/master/index.md).